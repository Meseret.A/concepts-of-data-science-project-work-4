## Creating AVL Tree of Numbers
```python
import pandas as pd
import requests
from io import StringIO

url = "https://gitlab.com/Meseret.A/concepts-of-data-science-project-work-4/-/raw/master/20_numbers.txt"
df = pd.read_csv(StringIO(requests.get(url).text), header=None)

numbers = df[0].values.tolist()

number_tree = AvlTree()
for i in numbers:
    number_tree.insert(i)

number_tree.draw_number_tree()
```

![svg](output_3_0.svg)

## Searching on the numbers file
```python
search_list = [13, 29, 48, 58]

for i in search_list:
    print(f'{i} is in the tree:', number_tree.search(i))
```

    13 is in the tree: False
    29 is in the tree: True
    48 is in the tree: True
    58 is in the tree: False


## Inserting on the numbers file
```python
insert_list = [5, 51, 82, 98]

print(number_tree.draw_number_tree())
for i in insert_list:
    number_tree.insert(i)
    print(f'inserting {i}...')
print(number_tree.draw_number_tree())
```

    
            ___________________49____________
           /                                 \
      ____29_________                     ____60______
     /               \                   /            \
    15            ____36            ____57         ____84
      \          /      \          /      \       /      \
       16      _31       43       54       59    67       87
              /   \        \        \              \        \
             30    34       48       56             71       99
    
    inserting 5...
    inserting 51...
    inserting 82...
    inserting 98...
    
              ___________________49_______________
             /                                    \
        ____29_________                        ____60_________
       /               \                      /               \
      15            ____36               ____57            ____84___
     /  \          /      \             /      \          /         \
    5    16      _31       43         _54       59      _71         _98
                /   \        \       /   \             /   \       /   \
               30    34       48    51    56          67    82    87    99
    

## Removing on the numbers file
```python
delete_list = [31,34,54,68]

print(number_tree.draw_number_tree())
for i in delete_list:
    number_tree.remove(i)
    print(number_tree.draw_number_tree())
```
              ___________________49_______________
             /                                    \
        ____29_________                        ____60_________
       /               \                      /               \
      15            ____36               ____57            ____84___
     /  \          /      \             /      \          /         \
    5    16      _31       43         _54       59      _71         _98
                /   \        \       /   \             /   \       /   \
               30    34       48    51    56          67    82    87    99

    "31" has been successfully removed
    
              ________________49_______________
             /                                 \
        ____29______                        ____60_________
       /            \                      /               \
      15            _36               ____57            ____84___
     /  \          /   \             /      \          /         \
    5    16      _34    43         _54       59      _71         _98
                /         \       /   \             /   \       /   \
               30          48    51    56          67    82    87    99
    
    "34" has been successfully removed
    
              _____________49_______________
             /                              \
        ____29___                        ____60_________
       /         \                      /               \
      15         _36               ____57            ____84___
     /  \       /   \             /      \          /         \
    5    16    30    43         _54       59      _71         _98
                       \       /   \             /   \       /   \
                        48    51    56          67    82    87    99
    
    "54" has been successfully removed
    
              _____________49____________
             /                           \
        ____29___                     ____60_________
       /         \                   /               \
      15         _36               _57            ____84___
     /  \       /   \             /   \          /         \
    5    16    30    43         _56    59      _71         _98
                       \       /              /   \       /   \
                        48    51             67    82    87    99
    
    "68" is not found in the tree
    
              _____________49____________
             /                           \
        ____29___                     ____60_________
       /         \                   /               \
      15         _36               _57            ____84___
     /  \       /   \             /   \          /         \
    5    16    30    43         _56    59      _71         _98
                       \       /              /   \       /   \
                        48    51             67    82    87    99
    

## Creating AVL Tree of Words
```python
url = "https://gitlab.com/Meseret.A/concepts-of-data-science-project-work-4/-/raw/master/2795_words.txt"
df = pd.read_csv(StringIO(requests.get(url).text), header=None)

words = df[0].values.tolist()

word_tree = AvlTree()

for i in words:
    word_tree.insert(str(i))

word_tree.draw_tree(22)
```

![svg](output_8_0.svg)
(Small part of the tree consisting of 2795 words)    


## Searching on the words file
```python
search_list = ["kangaroo", "harbor", "existentialism", "wheel"]

for i in search_list:
    print(f'"{i}" is in the tree:'+(15 - len(i))*" ", word_tree.search(i))
```

    "kangaroo" is in the tree:        False
    "harbor" is in the tree:          True
    "existentialism" is in the tree:  False
    "wheel" is in the tree:           True


## Inserting on the words file
```python
insert_list = ["afternoon", "maybe", "computer", "pigment"]

for i in insert_list:
    print(f'"{i}" is in the tree:'+(15 - len(i))*" ", word_tree.search(i))
    word_tree.insert(i)
    print(f'inserting {i}...')
    print(f'"{i}" is in the tree:'+(15 - len(i))*" ", word_tree.search(i), end="\n")
    print("\n")

```

    "afternoon" is in the tree:       False
    inserting afternoon...
    "afternoon" is in the tree:       True
    
    
    "maybe" is in the tree:           False
    inserting maybe...
    "maybe" is in the tree:           True
    
    
    "computer" is in the tree:        False
    inserting computer...
    "computer" is in the tree:        True
    
    
    "pigment" is in the tree:         False
    inserting pigment...
    "pigment" is in the tree:         True
    
    


# Removing on the words file
```python
delete_list = [ "persuade", "slippery", "drain", "shocking"]

for i in delete_list:
    if word_tree.search(i): 
        print(f'"{i}" is in the tree:'+(12 - len(i))*" ", word_tree.search(i))
        word_tree.remove(i)
        print(f'"{i}" is in the tree:'+(12 - len(i))*" ", word_tree.search(i))
    else:
        print(f'"{i}" is not found in the tree')
    print("\n")
```

    "persuade" is in the tree:     True
    "persuade" has been successfully removed...
    "persuade" is in the tree:     False
    
    
    "slippery" is in the tree:     True
    "slippery" has been successfully removed...
    "slippery" is in the tree:     False
    
    
    "drain" is in the tree:        True
    "drain" has been successfully removed...
    "drain" is in the tree:        False
    
    
    "shocking" is in the tree:     True
    "shocking" has been successfully removed...
    "shocking" is in the tree:     False


#ANALYSIS

## Time Complexity
***Insertion***

The insertion of a node is going to take O(log n) time because the tree is balanced. Also, the unbalances in the insertion process get fixed after a fixed number of rotations. So, the entire process is of O(log n) time.

```python
a = AvlTree()
a.insert(3)
a.insert(5)
a.insert(6)
a.insert(9)
a.insert(12)
a.insert(10)
a.draw_number_tree()
```
    
![svg](output_21_0.svg)

For inserting element 14, it must be inserted as right child of 12. Therefore, we need to traverse elements (in order 9, 12, 14) to insert 14 which has worst case complexity of O(log n).

***Remove***

Due to the balancing property of AVL Tree, the removal  process takes O(log n) in both the average and the worst cases as well as search and insert methods. 

```python
a.insert(14)
a.draw_number_tree()
```

![svg](output_24_0.svg)

For remove of element 3, we have to traverse elements to find 3 (in order 9, 5, 3). So remove in binary tree has worst case complexity of O(log n).

***Search***

For searching element 3, we have to traverse elements (in order 9, 5, 3) = 3 = log n. Therefore, searching in AVL tree has worst case complexity of O(log n). Here, we create a graph to compare the time complexities of linear search (*in* operator) and AVL search using the tree of words.  

```python
import timeit
import matplotlib.pyplot as plt

words_index = [10, 20, 40, 80, 160, 320, 640, 1280, 2560]

time_in = []
nr_runs = 5
for i in words_index:
    for j in range(nr_runs):
        t = timeit.timeit(lambda: words[i] in words)
    time_in.append(t/nr_runs)

time_search = []
nr_runs = 5
for i in words_index:
    for j in range(nr_runs):
        t = timeit.timeit(lambda: word_tree.search(words[i]))
    time_search.append(t/nr_runs)


plt.plot(words_index, time_in, label='Linear search')
plt.plot(words_index, time_search, label='AVL search ')
plt.legend()
```

![png](output_26_1.png)
